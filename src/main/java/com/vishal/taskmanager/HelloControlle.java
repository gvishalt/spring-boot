package com.vishal.taskmanager;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloControlle {

    @GetMapping("/hello")
    String hello(){
        return "hello";
    }
}
